import React from "react";
import clsx from "clsx";
import {
  createStyles,
  makeStyles,
  useTheme,
  Theme,
} from "@material-ui/core/styles";
import {
  AppBar,
  CssBaseline,
  Drawer,
  Divider,
  IconButton,
  List,
  Toolbar,
} from "@material-ui/core";

import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import AppsIcon from "@material-ui/icons/Apps";
import { Switch, Route, Link } from "react-router-dom";
import HomeIcon from "@material-ui/icons/Home";
import Home from "../Home";
import AddCaptions from "../ImageDetails/AddCaptions";
import Gallery from "../Gallery";
import EditImage from "../ImageDetails";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",

      border: 0,
      borderRadius: 3,
      boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
      color: "white",
      height: 48,
      padding: "0 30px",
    },
    appBar: {
      background: "linear-gradient(45deg, #FF512F 30%, #F09819 90%)",
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: "none",
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap",
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    iconColor: {
      "& a": {
        display: "flex",
        color: "#fff",
        textDecoration: "none",
        marginLeft: "6px",
        "& span": {
          position: "relative",
          bottom: "1px",
          left: "4px",
        },
      },
    },
    h1: {
      fontSize: "20px",
      fontWeight: "normal",
      "& span": {
        color: "#303030",
        fontWeight: "bold",
        textTransform: "capitalize",
        textShadow: "1px 1px 2px #fff",
      },
    },
  })
);

export default function MiniDrawer() {
  const classes: any = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <h1 className={classes.h1}>
            Automat<span>E</span>d cap<span>T</span>io<span>N</span> gener
            <span>A</span>tion of Images
          </h1>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItem className={classes.iconColor} button key="Home">
            <Link to="/">
              <HomeIcon />
              {Boolean(open) && <ListItemText primary={"HOME"} />}
            </Link>
          </ListItem>
          <ListItem className={classes.iconColor} button key="Gallery">
            <Link to="/gallery">
              <AppsIcon />
              {Boolean(open) && <ListItemText primary={"GALLERY"} />}
            </Link>
          </ListItem>
        </List>
        <Divider />
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />

        <Switch>
          <Route path="/gallery">
            <Gallery />
          </Route>

          <Route path="/image-details/:imageSet/:id">
            <EditImage />
          </Route>

          <Route path="/add-caption">
            <AddCaptions />
          </Route>

          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </main>
    </div>
  );
}
