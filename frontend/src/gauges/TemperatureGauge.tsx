import React from "react";
import {
  CircularGauge,
  Scale,
  Label,
  RangeContainer,
  Range,
  Title,
  Font,
  Export,
} from "devextreme-react/circular-gauge";

export default function TemperatureGauge(prop: any) {
  const firstOneThird = prop.max / 3;
  const secondOneThird = (prop.max / 3) * 2;

  return (
    <CircularGauge id="gauge" value={prop.value}>
      <Scale startValue={0} endValue={prop.max} tickInterval={prop.max / 10}>
        <Label useRangeColors={true} />
      </Scale>
      <RangeContainer palette="pastel">
        <Range startValue={0} endValue={firstOneThird} />
        <Range startValue={firstOneThird} endValue={secondOneThird} />
        <Range startValue={secondOneThird} endValue={prop.max} />
      </RangeContainer>
      <Title text={prop.title}>
        <Font size={28} />
      </Title>
      <Export enabled={true} />
    </CircularGauge>
  );
}
