import { useCallback, useState, useEffect } from "react";
import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LineChart from "./Charts/LineChart";
import TemperatureGauge from "./Gauges/TemperatureGauge";
import { Grid } from "@material-ui/core";
import axios from "axios";

const useStyles = makeStyles({
  h3: {
    fontWeight: "normal",
    margin: "0",
  },
  Icon: {
    marginLeft: "auto",
    width: "10%",
  },
  Paper: {
    margin: "auto",
    padding: 10,
    alignItems: "center",
    marginTop: 10,
  },
  PaperWhiteFilled: {
    height: "390px",
    background: "#eee",
    margin: "auto",
    padding: 10,
    alignItems: "center",
    marginTop: 10,
  },
});

export default function Home() {
  const classes = useStyles();
  const [historicData, setHistoricData] = useState([]);
  const [modelTrainedData, setModelTrainedData] = useState([]);
  const [loader, setLoader] = useState(true);

  const fetchData = useCallback(async () => {
    const result = await axios(
      `http://localhost:3001/search?index=trained_model_meta`
    );

    setHistoricData(result.data);
    setModelTrainedData(result.data[0]);
    setLoader(false);
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const labels = [
    { value: "loss", name: "Loss" },
    { value: "val_loss", name: "Val Loss" },
  ];

  const d: any = modelTrainedData;

  const loss: any =
    d._source && d._source.loss_curve && d._source.loss_curve[0].loss;
  const val_loss =
    d._source && d._source.loss_curve && d._source.loss_curve[1].val_loss;

  const stats =
    loss &&
    loss.map((item: any, index: any) => {
      return {
        country: `Epoch ${index}`,
        loss: item,
        val_loss: val_loss[index],
      };
    });

  const subtitles = loss && `For ${loss.length} Epochs`;

  const max = Math.max.apply(
    Math,
    historicData.map((o: any) => {
      const time = o._source.model_training_info.time_taken;
      const timeINT = time.split("TIME TOOK ")[1].split("MIN")[0];
      return parseFloat(timeINT);
    })
  );

  const latestModelData: any = historicData[0];
  const time =
    latestModelData && latestModelData._source.model_training_info.time_taken;
  const latestTime = time && time.split("TIME TOOK ")[1].split("MIN")[0];

  let total: any = 0;

  historicData.forEach((o: any) => {
    const time = o._source.model_training_info.time_taken;
    const timeINT = time.split("TIME TOOK ")[1].split("MIN")[0];
    total += parseFloat(timeINT);
  });

  const totalAvg = total / historicData.length;

  return (
    <Grid container spacing={3}>
      <Grid item sm={12} lg={12}>
        <Paper variant="outlined" className={classes.Paper}>
          <h3 className={classes.h3}>Aims:</h3>
          <ul>
            <li>
              Use a variety of datasets through machine learning in order to
              identify image content.
            </li>
            <li>
              To evaluate a range of ML methods for captioning and to
              investigate building the generator using convolutional neural
              networks. The techniques will also be using deep learning models
              in order to improve the caption accuracy.
            </li>
            <li>
              Showcase a working demo of the captions generated being read out
              using Amazon Polly or equivalent screen reading programmes for
              accessibility.
            </li>
          </ul>
        </Paper>
      </Grid>
      <Grid item sm={12} lg={12}>
        <Paper variant="outlined" className={classes.Paper}>
          <h3 className={classes.h3}>Methodology and outcome:</h3>
          <ul>
            <li>
              Images of varying types and complexity of the caption produced
              will be used to train the model.
            </li>
            <li>
              Images would be data labeled to extract and build up the captions.
              New images will be checked against our trained model to evaluate
              the quality of our captions (assisted learning).
            </li>
          </ul>
        </Paper>
      </Grid>
      <Grid item sm={6} lg={3}>
        <Paper variant="outlined" className={classes.Paper}>
          <Paper variant="outlined" className={classes.PaperWhiteFilled}>
            <LineChart
              loader={loader}
              title="Loss Validation function"
              stats={stats}
              labels={labels}
              subtitles={subtitles}
            />
          </Paper>
        </Paper>
      </Grid>
      <Grid item sm={3} lg={3}>
        <Paper variant="outlined" className={classes.Paper}>
          <Paper variant="outlined" className={classes.PaperWhiteFilled}>
            <TemperatureGauge
              max={max}
              title="Latest model training time"
              value={latestTime && parseFloat(latestTime)}
            />
          </Paper>
        </Paper>
      </Grid>
      <Grid item sm={6} lg={3}>
        <Paper variant="outlined" className={classes.Paper}>
          <Paper variant="outlined" className={classes.PaperWhiteFilled}>
            <TemperatureGauge
              max={max}
              title="Average training time"
              value={totalAvg}
            />
          </Paper>
        </Paper>
      </Grid>
      <Grid item sm={6} lg={3}>
        <Paper variant="outlined" className={classes.Paper}>
          <Paper variant="outlined" className={classes.PaperWhiteFilled}>
            <LineChart
              loader={loader}
              title="Loss Validation function"
              stats={stats}
              labels={labels}
              subtitles={subtitles}
            />
          </Paper>
        </Paper>
      </Grid>
    </Grid>
  );
}
