import {
  Chart,
  Series,
  ArgumentAxis,
  CommonSeriesSettings,
  Export,
  Legend,
  Margin,
  Title,
  Subtitle,
  Tooltip,
  Grid,
} from "devextreme-react/chart";
import CircularProgress from "@material-ui/core/CircularProgress";

interface LineChartProps {
  title: string;
  subtitles: string;
  labels: any;
  stats: any;
  loader: boolean;
}

export default function LineChart(props: LineChartProps) {
  const { loader, labels, stats, title, subtitles } = props;

  return (
    <>
      {!loader ? (
        <Chart palette="Violet" dataSource={stats && stats}>
          <CommonSeriesSettings argumentField="country" type={"line"} />
          {labels &&
            labels.map((item: any) => {
              return (
                <Series
                  key={item.value}
                  valueField={item.value}
                  name={item.name}
                />
              );
            })}
          <Margin bottom={20} />
          <ArgumentAxis
            valueMarginsEnabled={false}
            discreteAxisDivisionMode="crossLabels"
          >
            <Grid visible={true} />
          </ArgumentAxis>
          <Legend
            verticalAlignment="bottom"
            horizontalAlignment="center"
            itemTextPosition="bottom"
          />
          <Export enabled={true} />
          <Title text={title}>
            <Subtitle text={subtitles} />
          </Title>
          <Tooltip enabled={true} />
        </Chart>
      ) : (
        <CircularProgress color="secondary" />
      )}
    </>
  );
}
