import { useState } from "react";
import { Delete, Build, Hearing } from "@material-ui/icons";
import { Grid, Paper } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  fadeout: {
    display: "none",
  },
});

const styles: any = {
  Icon: {
    marginLeft: "auto",
  },
  Paper: {
    margin: "10px 0 0 0",
    padding: "0px 10px",
    display: "flex",
    alignItems: "center",
  },
};

function speak(text: any, callback: any) {
  const u = new SpeechSynthesisUtterance();
  u.text = text;
  u.lang = "en-UK";
  u.onend = function () {
    if (callback) {
      callback();
    }
  };
  u.onerror = function (e) {
    if (callback) {
      callback(e);
    }
  };
  speechSynthesis.speak(u);
}

export default function Todo(props: any) {
  const [fade, setFade] = useState(false);
  const classes = useStyles();

  const deleteTodo = () => {
    setFade(true);
    props.deleteTodo(props.index);
  };

  const gridClass = fade ? classes.fadeout : "";

  return (
    <Grid xs={12} className={gridClass} item key={props.index}>
      <Paper variant="outlined" style={styles.Paper}>
        <span>{props.todo}</span>
        <IconButton
          color="primary"
          aria-label="Edit"
          style={styles.Icon}
          onClick={() => props.updateTodo(props.index)}
        >
          <Build fontSize="small" />
        </IconButton>
        <IconButton color="secondary" aria-label="Delete" onClick={deleteTodo}>
          <Delete fontSize="small" />
        </IconButton>
        <IconButton
          color="primary"
          aria-label="Edit"
          onClick={() => speak(props.todo, null)}
        >
          <Hearing fontSize="small" />
        </IconButton>
      </Paper>
    </Grid>
  );
}
