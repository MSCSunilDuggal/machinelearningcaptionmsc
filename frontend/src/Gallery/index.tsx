import { useCallback, useState, useEffect } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import {
  Button,
  Card,
  CardActions,
  CardActionArea,
  CardContent,
  CardMedia,
  CircularProgress,
  Grid,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import Paging from "../Paging";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  })
);

export default function Gallery() {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1);
  const [loader, setLoader] = useState(true);
  const [dataSet, setDataSet] = useState("flickr8k");
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setLoader(true);
    setDataSet(event.target.value as string);
    fetchData(page, event.target.value);
  };

  const fetchData = useCallback(
    async (page: number, ds: any) => {
      const result = await axios(
        `http://localhost:3001/search?index=${ds ? ds : dataSet}&size=12&page=${
          page > 1 ? (page - 1) * 10 + 1 : 0
        }`
      );
      setData(result.data);
      setLoader(false);
    },
    [dataSet]
  );

  useEffect(() => {
    fetchData(page, null);
  }, [fetchData, page]);

  return (
    <Grid container spacing={1}>
      <Paging
        dataSet={dataSet}
        handleChange={handleChange}
        page={page}
        setPage={setPage}
        fetchData={fetchData}
        setLoader={setLoader}
      />
      <Grid container>&nbsp;</Grid>
      {loader ? (
        <Grid container direction="column" alignItems="center">
          <Grid item md={2}>
            <CircularProgress color="secondary" />
          </Grid>
        </Grid>
      ) : (
        <>
          <Grid container spacing={6}>
            {data.map((item: any) => (
              <Grid item md={2} key={item._id}>
                <Card className={classes.root}>
                  <CardActionArea
                    href={`/image-details/${item._index}/${item._id}`}
                  >
                    <CardMedia
                      component="img"
                      alt="Contemplative Reptile"
                      image={`data:image/jpg;base64, ${String.raw`${item._source.raw_data}`}`}
                      title="Contemplative Reptile"
                      height={200}
                    />
                    <CardContent>
                      <Typography gutterBottom variant="h6" component="h2">
                        {
                          item._source.name
                            .split("/")
                            [item._source.name.split("/").length - 1].split(
                              "."
                            )[0]
                        }
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                    <Button
                      color="secondary"
                      href={`/image-details/${item._index}/${item._id}`}
                    >
                      Captions
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
          <Grid container>&nbsp;</Grid>
          <Paging
            dataSet={dataSet}
            handleChange={handleChange}
            page={page}
            setPage={setPage}
            fetchData={fetchData}
            setLoader={setLoader}
          />
          <Grid container>&nbsp;</Grid>
        </>
      )}
    </Grid>
  );
}
