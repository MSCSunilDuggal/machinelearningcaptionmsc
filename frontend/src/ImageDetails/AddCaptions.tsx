import { useState } from "react";
import { Paper, Grid } from "@material-ui/core";
import AddTodoForm from "../ImageDetails/AddTodoForm";
import CaptionList from "../ImageDetails/CaptionList";

export default function AddCaptions(props: any) {
  const { listItems } = props;
  const [list, setList] = useState(listItems);

  const addToList = (todo: string) => {
    const currentlist: any = { ...list };
    currentlist[`todo${Date.now()}`] = {
      todo: todo,
      status: "active",
    };

    setList(currentlist);
  };

  const deleteTodo = (key: string) => {
    const currentlist: any = { ...list };
    currentlist[key] = null;
    setList(list);
  };

  const updateTodo = (key: string) => {
    const currentlist: any = { ...list };
    currentlist[key]["status"] = "editing";
    setList(currentlist);
  };

  const saveTodo = (key: string, todo: string) => {
    const currentlist: any = { ...list };
    currentlist[key] = {
      todo: todo,
      status: "active",
    };
    setList(currentlist);
  };

  return (
    <>
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper>
            <AddTodoForm addToList={addToList} />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <CaptionList
            deleteTodo={deleteTodo}
            list={list}
            updateTodo={updateTodo}
            saveTodo={saveTodo}
          />
        </Grid>
      </Grid>
    </>
  );
}
