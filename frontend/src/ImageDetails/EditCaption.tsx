import { createRef } from "react";
import { Save } from "@material-ui/icons";
import { Grid, IconButton, Input, Paper } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    form: {
      display: "flex",
    },
    Icon: {
      marginLeft: "auto",
      width: "10%",
    },
    Input: {
      padding: "10px",
      width: "90%",
    },
    Paper: {
      margin: "auto",
      padding: 10,
      alignItems: "center",
      marginTop: 10,
    },
  })
);

export default function EditCaption(props: any) {
  const inputRef: any = createRef();
  const classes = useStyles();

  return (
    <Grid xs={12} item key={props.index}>
      <Paper variant="outlined" className={classes.Paper}>
        <form
          onSubmit={() => {
            props.saveTodo(props.index, inputRef.current.value);
          }}
        >
          <Input
            className={classes.Input}
            defaultValue={props.todo}
            inputRef={inputRef}
          />
          <IconButton
            type="submit"
            color="primary"
            aria-label="Add"
            className={classes.Icon}
          >
            <Save fontSize="small" />
          </IconButton>
        </form>
      </Paper>
    </Grid>
  );
}
