import { useState, useEffect } from "react";
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import { useParams } from "react-router-dom";
import AddCaptions from "./AddCaptions";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import TemperatureGauge from "../Gauges/TemperatureGauge";

const useStyles = makeStyles({
  fadeout: {
    display: "none",
  },
  panelHolder: {
    background: "none",
    padding: "10px",
  },
  img: {
    display: "flex",
  },
  heading: {
    fontWeight: "normal",
    margin: 0,
    padding: "0 0 10px 0",
  },
  Paper: {
    margin: "auto",
    padding: 10,
    alignItems: "center",
    marginTop: 10,
    width: "100%",
  },
  PaperWhiteFilled: {
    height: "390px",
    background: "#eee",
    padding: 10,
    alignItems: "center",
    marginTop: 20,
  },
});

export default function EditImage() {
  const [data, setData] = useState({});
  const [captions, setCaptions] = useState([]);
  const [capResults, setCapResults] = useState([]);

  const params: any = useParams();
  const classes = useStyles();

  useEffect(() => {
    const fetchItem = async () => {
      const result = await axios(
        `http://localhost:3001/searchItem?index=${params.imageSet}&id=${params.id}`
      );

      setData(result.data);
      setCaptions(result.data._source.captions);
      setCapResults(result.data._source.cap_results);
    };
    fetchItem();
  }, [params.id, params.imageSet]);

  const itemData: any = data;

  const listItemsMapped =
    captions &&
    captions.length &&
    captions.map((caption: any) => {
      return {
        todo: caption,
        status: "active",
      };
    });

  const capResultsMapped =
    capResults &&
    capResults.length &&
    capResults.map((captionResult: any) => {
      return {
        todo: captionResult.caption
          .replace("startseq", "")
          .replace("endseq", ""),
        status: "active",
      };
    });

  let results = 0;
  let count = 0;

  if (capResults && capResults.length) {
    const x: any = capResults;
    const captionResult = x[x.length - 1].caption.toLowerCase().split(" ");

    captions.forEach((caption: any) => {
      const y = caption.toLowerCase().split(" ");
      const yPercentage = y.length / 100;
      const intersection = y.filter(
        (e: any) => captionResult.indexOf(e) !== -1
      );

      results = results + yPercentage * intersection.length * 100;
      count = count + 1;
    });

    results = results / count;
  }

  return (
    <>
      {captions.length && data && (
        <Grid container spacing={8}>
          <Grid item md={4}>
            <Paper variant="outlined" className={classes.panelHolder}>
              <h3 className={classes.heading}>
                Captions trained / to train with
              </h3>
              <AddCaptions listItems={listItemsMapped} />
            </Paper>
          </Grid>

          <Grid item md={4}>
            <Paper variant="outlined" className={classes.panelHolder}>
              <h3 className={classes.heading}>UUID: {itemData._source.uuid}</h3>
              <img
                className={classes.img}
                alt={itemData._source.uuid}
                src={`data:image/jpg;base64, ${
                  itemData && itemData._source && itemData._source.raw_data
                }`}
                title={itemData._source.uuid}
                width={"100%"}
              />
              <ul>
                <li>Height: {itemData._source.size[0]}px</li>
                <li>Width: {itemData._source.size[1]}px</li>
                <li>Image format: {itemData._source.image_format}</li>
              </ul>
            </Paper>
          </Grid>
          <Grid item md={4}>
            <Grid container>
              {capResults && capResults.length ? (
                <Paper variant="outlined" className={classes.panelHolder}>
                  <h3 className={classes.heading}>Results</h3>
                  <AddCaptions listItems={capResultsMapped} />
                </Paper>
              ) : (
                <Paper variant="outlined" className={classes.panelHolder}>
                  <h3 className={classes.heading}>Results</h3>
                  <p>
                    There has been no training done on this image and it's
                    captions.
                  </p>
                </Paper>
              )}
            </Grid>
            <Grid container>
              {capResults && capResults.length ? (
                <Paper variant="outlined" className={classes.Paper}>
                  <Paper
                    variant="outlined"
                    className={classes.PaperWhiteFilled}
                  >
                    <TemperatureGauge
                      max={100}
                      title="Average accuracy percentage"
                      value={results}
                    />
                  </Paper>
                </Paper>
              ) : (
                <Paper variant="outlined" className={classes.panelHolder}>
                  <h3 className={classes.heading}>Results</h3>
                  <p>
                    There has been no training done on this image and it's
                    captions.
                  </p>
                </Paper>
              )}
            </Grid>
          </Grid>
        </Grid>
      )}
    </>
  );
}
