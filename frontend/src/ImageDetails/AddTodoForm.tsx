import { createRef } from "react";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";

export default function AddTodoForm(props: any) {
  const inputRef: any = createRef();
  const errorRef: any = createRef();
  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (inputRef.current.value === "") {
      errorRef.current.classList.add("active");
      errorRef.current.style.removeProperty("display");
      return null;
    }
    errorRef.current.classList.remove("active");
    errorRef.current.style.display = "none";
    props.addToList(inputRef.current.value);
    e.currentTarget.reset();
  };

  return (
    <form onSubmit={handleSubmit} style={{ display: "flex", height: "40px" }}>
      <Input
        placeholder="Add caption"
        inputProps={{
          "aria-label": "Description",
        }}
        inputRef={inputRef}
        style={{ width: "90%", padding: "10px" }}
      />

      <Button
        type="submit"
        variant="contained"
        color="primary"
        style={{ width: "10%" }}
      >
        Add
      </Button>

      <p
        ref={errorRef}
        style={{
          height: "40px",
          padding: "0 10px",
          margin: 0,
          display: "none",
        }}
      >
        Error, please enter a caption.
      </p>
    </form>
  );
}
