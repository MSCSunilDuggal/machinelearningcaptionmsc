import React from "react";
import Pagination from "@material-ui/lab/Pagination";

export default function PaginationControlled(props: {
  page: number;
  setPage: Function;
  fetchData: Function;
  setLoader: Function;
}) {
  const { page, setPage, fetchData, setLoader } = props;

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    fetchData(value);
    setPage(value);
    setLoader(true);
  };

  return <Pagination count={8000 / 100} page={page} onChange={handleChange} />;
}
