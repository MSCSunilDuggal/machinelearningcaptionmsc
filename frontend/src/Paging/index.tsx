import { Grid, MenuItem, Select } from "@material-ui/core";
import PaginationControlled from "../Paging/PaginationControlled";

export default function Paging(props: {
  dataSet: any;
  handleChange: any;
  page: any;
  setPage: any;
  fetchData: any;
  setLoader: any;
}) {
  const { dataSet, handleChange, page, setPage, fetchData, setLoader } = props;

  return (
    <>
      <Grid sm={8} lg={9} item>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={dataSet}
          onChange={handleChange}
        >
          <MenuItem value={"coco"}>Coco</MenuItem>
          <MenuItem value={"flickr8k"}>Flicker 8k</MenuItem>
          <MenuItem value={"textcaps"}>Text Caps</MenuItem>
        </Select>
      </Grid>
      <Grid sm={4} lg={3} item>
        <PaginationControlled
          page={page}
          setPage={setPage}
          fetchData={fetchData}
          setLoader={setLoader}
        />
      </Grid>
    </>
  );
}
