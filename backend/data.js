const { Client } = require("@elastic/elasticsearch");
const config = require("config");
const elasticConfig = config.get("elastic");

const client = new Client({
  cloud: {
    id: elasticConfig.cloudID,
  },
  auth: {
    username: elasticConfig.username,
    password: elasticConfig.password,
  },
});

// client
//   .info()
//   .then((response) => console.log(response))
//   .catch((error) => console.error(error));

async function read() {
  const { body } = await client.search({
    index: "flickr8k",
  });
  console.log(body.hits.hits);
}

read().catch(console.log);
