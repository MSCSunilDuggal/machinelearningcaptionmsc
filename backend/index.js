const { Client } = require("@elastic/elasticsearch");
const config = require("config");
const elasticConfig = config.get("elastic");

const client = new Client({
  cloud: {
    id: elasticConfig.cloudID,
  },
  auth: {
    username: elasticConfig.username,
    password: elasticConfig.password,
  },
});

//require Express
const express = require("express");
// instanciate an instance of express and hold the value in a constant called app
const app = express();

//require the path library
const path = require("path");

// set port for the app to listen on
app.set("port", process.env.PORT || 3001);
// set path to serve static files
app.use(express.static(path.join(__dirname, "public")));
// enable CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// defined the base route and return with an HTML file called tempate.html
app.get("/", function (req, res) {
  res.sendFile("template.html", {
    root: path.join(__dirname, ""),
  });
});

async function read(bodyData) {
  const { body } = await client.search({
    ...bodyData,
  });
  return body.hits.hits;
}

// define the /search route that should return elastic search results
app.get("/search", function (req, res) {
  const bodyData = {
    sort: ["_id:asc"],
    index: req.query["index"] ? req.query["index"] : "flickr8k",
    size: req.query["size"],
    from: req.query["page"],
  };

  read(bodyData)
    .then((results) => {
      res.send(results);
    })
    .catch((err) => {
      console.log(err);
      res.send([]);
    });
});

async function readItem(bodyData) {
  const { body } = await client.get({
    ...bodyData,
  });
  return body;
}

// define the /search route that should return elastic search results
app.get("/searchItem", function (req, res) {
  const bodyData = {
    id: req.query["id"],
    index: req.query["index"] ? req.query["index"] : "flickr8k",
  };

  readItem(bodyData)
    .then((results) => {
      res.send(results);
    })
    .catch((err) => {
      console.log(err);
      res.send([]);
    });
});

// listen on the specified port
app.listen(app.get("port"), function () {
  console.log("Express server listening on port " + app.get("port"));
});
